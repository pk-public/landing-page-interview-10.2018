import truncate from './truncate'

export {
  truncate
}

export default function (Vue) {
  Vue.filter('truncate', truncate)
}
