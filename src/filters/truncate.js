export default (value, cut) => {
  if (value) {
    return value.toString().slice(0, cut) + (cut < value.length ? '...' : '')
  }
  return ''
}
