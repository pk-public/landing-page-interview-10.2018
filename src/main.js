import Vue from 'vue'
import App from './App'
import Filter from './filters'

Vue.use(Filter)

Vue.config.productionTip = false
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
