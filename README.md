# Landing page email with Pomodoro App

> Zadanie rekrutacyjne (4h)

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:8080
$ npm run dev

# build for production and launch server
$ npm run build
```
